window.onload = () => {
  console.log("Hello World!");
};

const tooltipTriggerList = document.querySelectorAll(
  '[data-bs-toggle="tooltip"]'
);
const tooltipList = [...tooltipTriggerList].map(
  (tooltipTriggerEl) => new bootstrap.Tooltip(tooltipTriggerEl)
);

const get_fact = async () => {
  let fetch_res = await fetch("https://meowfacts.herokuapp.com/");
  fetch_res = await fetch_res.json();

  let data = fetch_res["data"];
  //   console.log(data);

  let fact = data[0];

  document.getElementById("fact").innerText = fact;
};
